'use strict'
import React, { Component } from 'react';
import { View, Text ,Image,Platform,StyleSheet} from 'react-native';

import {Router,Scene, Actions,drawer} from 'react-native-router-flux'

import HomeScreen from './screens/HomeScreen'
import UserProfile from './screens/UserProfile'

import SettingScreen from './screens/SettingScreen'
import BlueScreen from './screens/BlueScreen'

import NotificationScreen from './screens/NotificationScreen'
import GreenScreen from './screens/GreenScreen'

import {Content,List,ListItem,Button,Icon} from 'native-base'

export default class Drawer extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <Router navigationBarStyle={{backgroundColor:'#F7941D'}}>
          <Scene key='root'>
            <drawer
              key='drawer'
              drawer
              contentComponent={Menu}
              drawerWidth={300}
              renderLeftButton={() => 
                <Button transparent onPress={()=>Actions.drawerOpen()}>
                  <Image source={require('./img/humburger.png')} 
                  style={{ width: 25, height: 25,marginLeft:15,marginTop:Platform.OS=='android'? 10:0}} />
                </Button>}
                renderRightButton={() => 
                <Button transparent onPress={()=>Actions.notification()}>
                  <Image source={require('./img/icons8-alarm-filled-100.png')} 
                  style={{ width: 25, height: 25,marginRight:15,marginTop:Platform.OS=='android'? 10:0}} />
                </Button>}  
              hideNavBar
              initial
              
            >
              <Scene key="home" 
              renderTitle={()=>(
              <View>
                <Image source={require('./img/logo-kd-txt.png')} style={{ width: 125, height: 15}} />
              </View>
              )}
              titleStyle={{color:'white',fontSize:20}}
              component={HomeScreen}/>
              <Scene 
              key="setting" 
              title='Setting' 
              titleStyle={{color:'white',fontSize:20}}
              component={SettingScreen} 
              back 
              renderBackButton={()=>
                <Button transparent onPress={()=>Actions.home()}>
                  <Image source={require('./img/icons8-left-100-white.png')} style={{ width: 20, height: 20,marginLeft:15}} />
                </Button>
              }
              renderRightButton={() => 
                <Button transparent></Button>
              }
              />
              <Scene 
              key="notification" 
              title="Notification" 
              titleStyle={{color:'white',fontSize:20}}
              component={NotificationScreen} 
              back
              renderBackButton={()=>
                <Button transparent onPress={()=>Actions.home()}>
                  <Image source={require('./img/icons8-left-100-white.png')} 
                  style={{ width: 20, height: 20,marginLeft:15,marginTop:Platform.OS=='android'? 10:0}} />
                </Button>
              }
              renderRightButton={() => 
                <Button transparent></Button>
              }
              />
             
            </drawer>
          </Scene>
      </Router>
    );
  }
}

const Menu=()=>{
  return(
    <View style={{ flex: 1 ,paddingTop:Platform.OS=='ios'?30:0,backgroundColor:'#F7941D'}}>
                <View style={{ flex:1,justifyContent: 'center',padding:Platform.OS=='android'? 10:0 ,alignItems: 'center', backgroundColor:'#F7941D'}}>
                    <View style={{width: 100, height: 100 ,backgroundColor:'#fff',borderRadius:50,
                      borderWidth:1,borderColor:'#fff'}}>
                      <Image source={require('./img/kd-circle1.png')} 
                      style={{ width: "100%", height: "100%" ,}} 
                      />
                    </View>
                    <Text 
                    style={{ fontSize:16,fontWeight:'bold',marginTop:12,color:'white'}} >
                    Name : Sorn Veng e
                    </Text>
                    <Text style={{fontSize:13,color:'#eee'}}>
                      sornvenge@gmail.com
                    </Text>
                </View>
                <View style={{ flex: 3,backgroundColor:'#fff',paddingTop:10 }}>
                    <Content>
                        <List>
                            <ListItem onPress={()=>Actions.drawerClose()}>
                              <Image 
                                source={require('./img/icons8-home-filled-100.png')} 
                                style={styles.menuIcon}
                              />  
                              <Text 
                                
                                style={styles.menuText}
                              >HOME
                              </Text>
                            </ListItem>
                            <ListItem>
                              <Image 
                                source={require('./img/icons8-user-100.png')} 
                                style={styles.menuIcon}
                              />
                              <Text style={styles.menuText}>USER PROFILE</Text>
                            </ListItem>
                            <ListItem>
                              <Image 
                                source={require('./img/icons8-geography-filled-100.png')} 
                                style={styles.menuIcon}
                              />
                              <Text style={styles.menuText}>REQUEST WEBSITE</Text>
                            </ListItem>
                            <ListItem onPress={()=>Actions.setting()}>
                              <Image 
                                source={require('./img/icons8-services-480.png')} 
                                style={styles.menuIcon}
                              />
                              <Text                               
                                style={styles.menuText}
                                >SETTING
                              </Text>
                            </ListItem>
                            <ListItem>
                              <Image 
                                source={require('./img/icons8-login-100.png')} 
                                style={styles.menuIcon}
                              />
                              
                              <Text style={styles.menuText}>LOG IN</Text>
                            </ListItem>
                        </List>
                    </Content>
                </View>
            </View>

  )
}
const styles = StyleSheet.create({
  menuText:{
    color:'#555555',
    fontSize:13,
    fontWeight: 'bold',
  },
  menuIcon:{
    width:20,
    height:20,
    marginRight: 30
  }
})
