import React, { Component } from 'react';
import { View, Text ,StyleSheet,Image,TextInput,Platform} from 'react-native';
import {Actions} from 'react-native-router-flux'
import { 
  Container, 
  Header, 
  Item, 
  Button,
  Card,
  CardItem,
  Left,
  Right,
  Body,
} from 'native-base';
import SearchBar from '../reuseableComponent/SearchBar';

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <Container>
        {/* Logo */}
        <View style={styles.logo}>
          <Image source={require('../img/logo-KD-small.png')} style={{ width: '100%', height: '100%'}} />
        </View>
        {/* Search bar */}
       <SearchBar/>
        
        {/* Body category */}
        <View style={[styles.shadow,styles.boxWrapper]}>
        {/* box cate */}
          <View style={[styles.fifty,styles.box]}>          
            <View>
              <Image 
              source={require('../img/icons8-heart-with-pulse-100.png')} 
              style={{ width: 65, height: 65,}}  />
            </View>
            <View>
              <Text>Healthy</Text>
            </View> 
            <View style={{margin:-10}}>
              <Button transparent>
                <Image 
                source={require('../img/icons8-expand-arrow-100.png')} 
                style={styles.btnDown} />
              </Button>
            </View>  
          </View>
          <View style={[styles.fifty,styles.box]}>          
            <View>
              <Image 
              source={require('../img/icons8-heart-with-pulse-100.png')} 
              style={{ width: 65, height: 65,}}  />
            </View>
            <View>
              <Text>Healthy</Text>
            </View> 
            <View style={{margin:-10}}>
              <Button transparent>
                <Image 
                source={require('../img/icons8-expand-arrow-100.png')} 
                style={styles.btnDown} />
              </Button>
            </View>  
          </View>
          <View style={[styles.fifty,styles.box]}>          
            <View>
              <Image 
              source={require('../img/icons8-heart-with-pulse-100.png')} 
              style={{ width: 65, height: 65,}}  />
            </View>
            <View>
              <Text>Healthy</Text>
            </View>
            <View style={{margin:-10}}>
              <Button transparent>
                <Image 
                source={require('../img/icons8-expand-arrow-100.png')} 
                style={styles.btnDown} />
              </Button>
            </View>
          </View>
          <View style={[styles.fifty,styles.box]}>          
            <View>
              <Image 
              source={require('../img/icons8-heart-with-pulse-100.png')} 
              style={{ width: 65, height: 65,}}  />
            </View>
            <View>
              <Text>Healthy</Text>
            </View> 
            <View style={{margin:-10}}>
              <Button transparent>
                <Image 
                source={require('../img/icons8-expand-arrow-100.png')} 
                style={styles.btnDown} />
              </Button>
            </View>  
          </View>
        </View>
        </Container>
    );
  }
}

const styles= StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#fff',
    flexDirection: 'column',
  },
  shadow:{
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
  },
  search:{
    padding:25,backgroundColor:'white',
  },
  logo:{
    width:100,height:100,alignSelf:'center',marginTop:20
  },
  fifty:{
    width:'48%'
  },
  boxWrapper:{
    flex:1,margin:5,marginTop:10,
    flexDirection:'row',
    flexWrap: 'wrap',
  },
  box:{
    backgroundColor:"#fff",
    justifyContent:'center',
    alignItems: 'center',
    height:180,
    margin:'1%',  
    borderColor:'whitesmoke',
    borderRadius:5,
    borderWidth:Platform.OS==='android'?3:0
  },
  btnDown:{
    width: 18, height: 18,borderColor:'#F7941D',borderWidth:1,borderRadius:9
  },
 
})
